from bottle import Bottle, run, debug
from service import server as serviceserver
#from client import server as serviceclient

from ConfigParser import ConfigParser

if __name__ == '__main__':
    import sys

    if len(sys.argv) > 1:
        configfile = sys.argv[1]
    else:
        configfile = 'config.ini'

    config = ConfigParser()
    print 'loading config from %s...' % configfile
    config.read(configfile)
    app = Bottle()
    debug(True)
    serviceserver.serve(config.get('webservice', 'server'),
          config.get('webservice', 'port'),
          config.get('couchdb', 'server'),
          config.get('couchdb', 'database'),'/service', app)
    #serviceclient.serve('', 'http://%s:%s/service' % (config.get('webservice', 'server'), 
    #    config.get('webservice', 'port')), app)

    #print 'serving database %s from %s' % (db , '%s:%s' % (server, port))
    run(app, host=config.get('webservice', 'server'), port=config.get('webservice', 'port'), reloader=True)
