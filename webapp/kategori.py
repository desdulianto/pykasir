from bottle import Bottle, run, debug, response, static_file
from mako.lookup import TemplateLookup
import urllib2
import json
from model import Kategori as kat
import couchdb

class Kategori(object):
    def __init__(self, root, app):
        self.root = root 
        self.app = app

        #self.app.route('%s' % self.root, method='GET')(self.index)
        #self.app.route('%s/<meja>' % self.root, method='GET')(self.makeOrder)
        #self.app.route('/static/<filepath:path>', method='GET')(self.static)
        self.app.route('/kategori', method='GET')(self.list_view)
        self.app.route('/kategori/<docid>', method='GET')(self.detail_view)

    def static(self, filepath):
        return static_file(filepath, root='static/')

    def list_view(self):
        columns = ['id', 'nama', 'parent']
        db = couchdb.Server('http://localhost:5984')['chatterbox']
        try:
            kategori = kat.view(db, '_design/all/_view/kategori', include_docs=True)
        except Exception as e:
            response.status = '500 Internal Server Error'
            return e.message

        rows = []
        for row in kategori:
            rows.append(row)
        mylookup = TemplateLookup(directories=['templates'], module_directory='templates/compiled')
        return mylookup.get_template('list.tpl').render(cols=columns,rows=rows,judul='Kategori')

    def detail_view(self, docid):
        db = couchdb.Server('http://localhost:5984')['chatterbox']
        kategori = kat.load(db, docid)
        if kategori is None:
            response.status = '404 Not Found'
            return 'Kategori %s not found' % docid
        fields = ('id', 'nama', 'parent')
        mylookup = TemplateLookup(directories=['templates'], module_directory='templates/compiled')
        return mylookup.get_template('detail.tpl').render(fields=fields, data=kategori, judul='Kategori: %s (%s)' % (kategori.nama, kategori.id))

    #def update_view(self, docid):
    #    db = couchdb.Server('http://localhost:5984')['chatterbox']
    #    kategori = kat.load(db, docid)
    #    if kategori is None:
    #        response.status = '404 Not Found'
    #        return 'Kategori %s not found' % docid
    #    fields = ('id', 'nama', 'parent')

    def index(self):
        #response.content_type = 'text/html'
        #response.status = '200 OK'

        #try:
        #    req = urllib2.Request(self.service + '/meja', headers={'Content-Type':'application/json'})
        #    resp = urllib2.urlopen(req)

        #    body = '<ol>'
        #    for i in json.load(resp):
        #        body = body + '<li><a href="%s">%s</a></li>' % (self.root + '/' + i['nama'], i['nama'])
        #    body = body + '</ol>'
        #except Exception as e:
        #    body = e.message

        mylookup = TemplateLookup(directories=['templates'], module_directory='templates/compiled')
        hasil = mylookup.get_template('order.tpl')
        return hasil.render(judul="Order")

    def makeOrder(self, meja):
        data = {}
        data['apptitle']='Chatterbox Restaurant System',
        data['pagetitle'] = 'Order'
        data['copyright']='MIT &copy; 2012'

        data['body'] = '<ul>'
        # get menu
        req = urllib2.Request(self.service + '/menu', headers={'Content-Type':'application/json'})
        resp = urllib2.urlopen(req)
        for i in json.load(resp):
            data['body'] = data['body'] + '<li><a href=''>%s</a></li>' % i['nama']
        data['body'] = data['body'] + '</ul>'

        return Template(filename='templates/base.tpl').render(**data)

if __name__ == '__main__':
    app = Bottle()
    debug(True)
    Order('/order', app)
    run(app, host='localhost', port='8080', reloader=True)
