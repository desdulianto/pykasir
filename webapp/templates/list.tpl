## order.tpl
<%inherit file="base.tpl"/>
<style type="text/css">
    table td { border: 1px solid black; }
</style>
<table>
    <tr>
% for col in cols:
    <th>${col}</th>
% endfor
    </tr>
% for row in rows:
    <tr>
% for key in cols:
        <td>${getattr(row, key)}</td>
% endfor
    </tr>
% endfor
</table>
<%block name="head"><title>testing</title></%block>
<%block name="header"><h1>${judul}</h1></%block>
