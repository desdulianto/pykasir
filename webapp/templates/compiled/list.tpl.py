# -*- encoding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 8
_modified_time = 1346342902.120049
_enable_loop = True
_template_filename = 'templates/list.tpl'
_template_uri = 'list.tpl'
_source_encoding = 'ascii'
_exports = [u'header', u'head']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'base.tpl', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def head():
            return render_head(context.locals_(__M_locals))
        rows = context.get('rows', UNDEFINED)
        judul = context.get('judul', UNDEFINED)
        cols = context.get('cols', UNDEFINED)
        def header():
            return render_header(context.locals_(__M_locals))
        getattr = context.get('getattr', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 2
        __M_writer(u'\n<style type="text/css">\n    table td { border: 1px solid black; }\n</style>\n<table>\n    <tr>\n')
        # SOURCE LINE 8
        for col in cols:
            # SOURCE LINE 9
            __M_writer(u'    <th>')
            __M_writer(unicode(col))
            __M_writer(u'</th>\n')
        # SOURCE LINE 11
        __M_writer(u'    </tr>\n')
        # SOURCE LINE 12
        for row in rows:
            # SOURCE LINE 13
            __M_writer(u'    <tr>\n')
            # SOURCE LINE 14
            for key in cols:
                # SOURCE LINE 15
                __M_writer(u'        <td>')
                __M_writer(unicode(getattr(row, key)))
                __M_writer(u'</td>\n')
            # SOURCE LINE 17
            __M_writer(u'    </tr>\n')
        # SOURCE LINE 19
        __M_writer(u'</table>\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'head'):
            context['self'].head(**pageargs)
        

        # SOURCE LINE 20
        __M_writer(u'\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        # SOURCE LINE 21
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        judul = context.get('judul', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 21
        __M_writer(u'<h1>')
        __M_writer(unicode(judul))
        __M_writer(u'</h1>')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_head(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def head():
            return render_head(context)
        __M_writer = context.writer()
        # SOURCE LINE 20
        __M_writer(u'<title>testing</title>')
        return ''
    finally:
        context.caller_stack._pop_frame()


