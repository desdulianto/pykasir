from bottle import Bottle, run, debug, get, route
from beaker.middleware import SessionMiddleware
from order import Order
from kategori import Kategori

#def serve(root, serviceroot, app):
#    Order('%s/order' % root, serviceroot, app)

def home(path, app):
    @app.get(path=path)
    def index():
        return 'Hello world'

def main(host, port, root):
    session_opts = {
        'session.type': 'file',
        'session.cookie_expires': 300,
        'session.data_dir' : './sessions',
        'session.auto': True
    }
    bottleApp = Bottle()
    app = SessionMiddleware(bottleApp, session_opts)

    #home(root, bottleApp)
    Kategori('/', bottleApp)

    debug(True)
    run(app, host=host, port=port, reloader=True)
    return app

if __name__ == '__main__':
    #app = Bottle()
    #serve('', 'http://localhost:8080/service', app)
    #debug(True)
    #run(app, host='localhost', port='8081', reloader=True)
    main(host='localhost', port='8081', root='/')
