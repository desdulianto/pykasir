from bottle import Bottle, run, debug
from order import Order

def serve(root, serviceroot, app):
    Order('%s/order' % root, serviceroot, app)

if __name__ == '__main__':
    app = Bottle()
    serve('', 'http://localhost:8080/service', app)
    debug(True)
    run(app, host='localhost', port='8081', reloader=True)
