from bottle import Bottle, run, debug, response, static_file
from mako.lookup import TemplateLookup
import urllib2
import json

class Order(object):
    def __init__(self, root, service, app):
        self.root = root 
        self.app = app
        self.service = service

        self.app.route('%s' % self.root, method='GET')(self.index)
        self.app.route('%s/<meja>' % self.root, method='GET')(self.makeOrder)
        self.app.route('/static/<filepath:path>', method='GET')(self.static)

    def static(self, filepath):
        return static_file(filepath, root='static/')

    def index(self):
        #response.content_type = 'text/html'
        #response.status = '200 OK'

        #try:
        #    req = urllib2.Request(self.service + '/meja', headers={'Content-Type':'application/json'})
        #    resp = urllib2.urlopen(req)

        #    body = '<ol>'
        #    for i in json.load(resp):
        #        body = body + '<li><a href="%s">%s</a></li>' % (self.root + '/' + i['nama'], i['nama'])
        #    body = body + '</ol>'
        #except Exception as e:
        #    body = e.message

        mylookup = TemplateLookup(directories=['templates'], module_directory='templates/compiled')
        hasil = mylookup.get_template('order.tpl')
        return hasil.render(judul="Order")

    def makeOrder(self, meja):
        data = {}
        data['apptitle']='Chatterbox Restaurant System',
        data['pagetitle'] = 'Order'
        data['copyright']='MIT &copy; 2012'

        data['body'] = '<ul>'
        # get menu
        req = urllib2.Request(self.service + '/menu', headers={'Content-Type':'application/json'})
        resp = urllib2.urlopen(req)
        for i in json.load(resp):
            data['body'] = data['body'] + '<li><a href=''>%s</a></li>' % i['nama']
        data['body'] = data['body'] + '</ul>'

        return Template(filename='templates/base.tpl').render(**data)

if __name__ == '__main__':
    app = Bottle()
    debug(True)
    Order('/order', app)
    run(app, host='localhost', port='8080', reloader=True)
