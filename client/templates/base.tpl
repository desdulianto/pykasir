<html>
    <head>
        <link rel="stylesheet" href="static/css/base.css" />
        <%block name="head"></%block>
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
            <%block name="header"></%block>
            </div>
            <div id="body">
            ${self.body()}
            </div>
            <div id="footer">
            <%block name="footer"></%block>
            </div>
        </div>
    </body>
</html>
