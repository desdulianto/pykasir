from bottle import request, response
import json
import traceback

class CRUDWebService(object):
    def __init__(self, model, path, app):
        self.path = path
        self.app = app
        self.model = model
        #self.content_type = 'application/json'
        #response.content_type = 'application/json'
        # routes
        self.app.route(self.path, method='POST')(self.create)
        self.app.route(self.path, method='GET')(self.readAll)
        self.app.route('%s/<itemID:re:[a-z0-9]+>' % self.path, method='GET')(self.readOne)

    def _checkRequestHeader(self):
        if 'application/json' not in request.headers['Content-Type']:
            raise Exception('Request Content-Type must be application/json')

    def create(self):
        '''service untuk buat object baru'''
        self._checkRequestHeader()

        data = request.json

        response.content_type = 'application/json'
        try:
            tb = None
            item = self.model.create(**data)
            response.status = '201 Created'
            return json.dumps(item)
        except Exception as e:
            response.status = '500 Internal Server Error'
            tb = traceback.format_exc()
            #return json.dumps({'error': e.message, 'stack trace': tb})
            return json.dumps({'error': e.message})
        finally:
            if tb:
                print tb

    def readAll(self):
        '''service untuk menampilkan daftar object'''
        self._checkRequestHeader()

        response.content_type = 'application/json'
        try:
            items = self.model.read('_design/all/_view/%s' % self.model.type, include_docs=True)
            response.status = '200 OK'
            return json.dumps([item.unwrap() for item in items])
        except Exception as e:
            response.status = '500 Internal Server Error'
            return json.dumps({'error': e.message})

    def readOne(self, itemID):
        '''service untuk menampilkan satu object'''
        self._checkRequestHeader()

        response.content_type = 'application/json'
        try:
            item = self.model.readOne(itemID)
            if item is not None:
                response.status = '200 OK'
                return json.dumps(item.unwrap())
            else:
                response.status = '404 Not Found'
        except Exception as e:
            response.status = '500 Internal Server Error'
            return json.dumps({'error': e.message})
