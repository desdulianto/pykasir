from crud import CRUDObject
from webservice import CRUDWebService 
from bottle import Bottle, run, debug
import model
import couchdb
from decimal import Decimal
import types
import functools
from utils import noneOrEmpty, exist, noneOrZero
import hashlib

from ConfigParser import ConfigParser

methodType = types.MethodType

def serve(server, port, dbserver, db, root, app):
    # couchdb database connection
    dbconn = couchdb.Server(dbserver)

    # curry function for building CRUDObject
    chatterboxCRUD = functools.partial(CRUDObject, database=db, connection=dbconn)
    chatterboxWebService = functools.partial(CRUDWebService, app=app)

    # build CRUD objects and WebServices Objects
    cruds = {}
    rests = {}
    for i in ('kategori', 'menu', 'order', 'pembayaran', 'meja', 'user', 'group', 'permission'):
        cruds[i] = chatterboxCRUD(i.title())
        rests[i] = chatterboxWebService(cruds[i], '%s/%s' % (root, i))

    def validateMejaCreate(self, model):
        if noneOrEmpty(model, 'nama'):
            raise Exception('Nama meja tidak boleh kosong')
    cruds['meja']._preCreate = methodType(validateMejaCreate, cruds['meja'])

    def validateKategoriCreate(self, model):
        if noneOrEmpty(model, 'nama'):
            raise Exception('%s tidak boleh kosong' % 'nama')

        if (model.parent is not None and not 
            exist(self.db, model.parent, 'kategori')):
            raise Exception('Kategori parent %s tidak ditemukan' % model.parent)
    cruds['kategori']._preCreate = \
        methodType(validateKategoriCreate, cruds['kategori'])

    def validateMenuCreate(self, model):
        if noneOrEmpty(model, 'nama'):
            raise Exception('%s tidak boleh kosong' % 'nama')

        if (model.kategori is None or 
            not exist(self.db, model.kategori, 'kategori')):
            raise Exception('Kategori %s tidak ditemukan' % model.kategori)

        for i in ('normal', 'member'):
            harga = getattr(model.harga, i, None)
            if harga is None and i != 'normal':
                continue
            if harga < Decimal(0):
                raise Exception('Harga %s tidak boleh < 0' % i)
    cruds['menu']._preCreate = methodType(validateMenuCreate, cruds['menu'])

    def validateOrderCreate(self, model):
        if noneOrEmpty(model, 'meja'):
            raise Exception('%s tidak boleh kosong' % 'meja')

        if len(model.details) == 0:
            raise Exception('Detail order tidak boleh kosong')

        # validasi masing2 detail
        for i in model.details:
            if not exist(self.db, i.id, 'menu'):
                raise Exception('Tidak ada menu dengan id %s' % i.id)
            if i.qty <= Decimal(0):
                raise Exception('Qty order tidak boleh <= 0')
            # hapus semua data yang tidak diperboleh pada detail order
            # pada saat pembuatan order
            i.voided = None
            i.voidedBy = None
            i.modified = None
            i.modifiedBy = None
    cruds['order']._preCreate = methodType(validateOrderCreate, cruds['order'])

    def validatePembayaranCreate(self, model):
        # harus ada orderID
        if noneOrEmpty(model, 'orderID'):
            raise Exception('orderID tidak boleh kosong')

        if noneOrEmpty(model, 'orderRev'):
            raise Exception('orderRev tidak boleh kosong')

        # ambil nomor order
        order = self.mdl.Order.load(self.db, model.orderID)
        
        # jika revisi order tidak match raise error, order updated
        if order.rev != model.orderRev:
            raise Exception('Order telah diubah')

        if len(model.details) == 0:
            raise Exception('Detail pembayaran harus diisi')

        for detail in model.details:
            if detail.get('caraBayar', None) not in ('tunai', 'voucher', 'kartu kredit'):
                raise Exception('Cara pembayaran harus tunai/voucher/kartu kredit')
            # voucher dan kartu kredit harus memiliki no kartu
            if detail['caraBayar'] in ('voucher', 'kartu kredit') and noneOrEmpty(detail, 'noKartu'):
                raise Exception('No Voucher/Kartu Kredit harus diisi')
            # kartu kredit harus memiliki bank penerbit kartu
            if detail['caraBayar'] == 'kartu kredit' and noneOrEmpty(detail, 'bank'):
                raise Exception('Nama Bank penerbit kartu harus diisi')
            # kartu kredit harus memiliki jenis kartu
            if detail['caraBayar'] == 'kartu kredit' and noneOrEmpty(detail, 'jenisKartu'):
                raise Exception('Jenis Kartu Kredit harus diisi')
            # harus ada jumlah
            if noneOrZero(detail, 'jumlah'):
                raise Exception('Jumlah harus diisi dan harus > 0')

        jumlahBayar = model.jumlahBayar()
        jumlahTagihan = order.jumlahTagihan()

        if jumlahBayar < jumlahTagihan:
            raise Exception('Jumlah pembayaran tidak mencukupi')

    def afterPembayaranCreate(self, model):
        order = CRUDObject('Order', self.database, self.connection)
        order.update(model.orderID, model.orderRev, pembayaranID=model.id,pembayaranRev=model.rev)
    cruds['pembayaran']._preCreate = methodType(validatePembayaranCreate, cruds['pembayaran'])
    cruds['pembayaran']._postCreate = methodType(afterPembayaranCreate, cruds['pembayaran'])


    def beforeUserCreate(self, model):
        if noneOrEmpty(model, 'nama'):
            raise Exception('Nama tidak boleh kosong')

        if noneOrEmpty(model, 'password'):
            raise Exception('Password tidak boleh kosong')

        model.password = hashlib.sha1(model.password).hexdigest()

        if (noneOrEmpty(model, 'group') or not 
            exist(self.db, model.group, 'group')):
            raise Exception('Group tidak terdaftar')

    cruds['user']._preCreate = methodType(beforeUserCreate, cruds['user'])

    def beforeGroupCreate(self, model):
        if noneOrEmpty(model, 'nama'):
            raise Exception('Nama tidak boleh kosong')
    cruds['group']._preCreate = methodType(beforeGroupCreate, cruds['group'])

    def beforePermissionCreate(self, model):
        if (noneOrEmpty(model, 'subject') or 
            (not exist(self.db, model.subject, 'user')
            and not exist(self.db, model.subject, 'group'))):
            raise Exception('User/Group tidak terdaftar')

        if noneOrEmpty(model, 'action'):
            raise Exception('Action tidak boleh kosong')
    cruds['permission']._preCreate = methodType(beforePermissionCreate, cruds['permission'])


if __name__ == '__main__':
    import sys

    if len(sys.argv) > 1:
        configfile = sys.argv[1]
    else:
        configfile = 'config.ini'

    config = ConfigParser()
    print 'loading config from %s...' % configfile
    config.read(configfile)
    app = Bottle()
    debug(True)
    serve(config.get('webservice', 'server'),
          config.get('webservice', 'port'),
          config.get('couchdb', 'server'),
          config.get('couchdb', 'database'),app)

    #print 'serving database %s from %s' % (db , '%s:%s' % (server, port))
    run(app, host=config.get('webservice', 'server'), port=config.get('webservice', 'port'), reloader=True)
