import couchdb
from decimal import Decimal

def exist(db, docid, doctype):
    try:
        return db[docid]['type'] == doctype
    except couchdb.ResourceNotFound as e:
        return False

def noneOrEmpty(model, attr):
    '''kembalikan nilai True apabila attribute model tidak ditemukan None atau kosong,
        False sebaliknya'''
    item = getattr(model, attr, None)
    return item is None or item.strip() == ''

def noneOrZero(model, attr):
    '''kembalikan nilai True jika attribute model tidak ditemukan None atau atau 0 (nol),
        False sebaliknya'''
    item = getattr(model, attr, None)
    return item is None or item == Decimal(0)

def betweenIn(a, b, c):
    '''c between a and b inclusively (a <= c <= b)'''
    return a <= c <= b

def betweenEx(a, b, c):
    '''c between a and b exclusively (a < c < b)'''
    return a < c < b
