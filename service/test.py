import unittest
import urllib2
import json
import couchdb
import functools

class WebServiceTest(unittest.TestCase):
    def setUp(self):
        self.database = 'chatterboxtest'
        self.server = 'http://localhost:5984'
        self.webserver = 'localhost'
        self.webport = '8080'

        self.db = couchdb.Server(self.server)[self.database]

    def tearDown(self):
        # delete all of database records
        for i in self.db.view('_all_docs'):
            del self.db[i.id]

    def test_kategori_create(self):
        # kategori create
        url = 'http://localhost:8080/service'
        headers = {'Content-Type': 'application/json'}
        req = functools.partial(urllib2.Request, url=url+'/kategori', headers=headers)

        data = json.dumps(dict(nama='makanan', id='makanan'))

        # create succeed
        x = req(data=data)
        f = urllib2.urlopen(x)
        self.assertTrue(f.code == 201)

        # conflict on create
        self.assertRaises(urllib2.HTTPError, urllib2.urlopen, x)

        # no id
        data = json.dumps(dict(nama='makanan'))
        x = req(data=data)
        self.assertRaises(urllib2.HTTPError, urllib2.urlopen, x)

        # no nama
        data = json.dumps(dict(id='makanan'))
        x = req(data=data)
        self.assertRaises(urllib2.HTTPError, urllib2.urlopen, x)

        # parent
        data = json.dumps(dict(id='nasi', nama='nasi', parent='makanan'))
        x = req(data=data)
        self.assertTrue(f.code == 201)

        # parent tidak ada di dalam database
        data = json.dumps(dict(id='juice', nama='juice', parent='minuman'))
        x = req(data=data)
        self.assertRaises(urllib2.HTTPError, urllib2.urlopen, x)

if __name__ == '__main__':
    unittest.main()
