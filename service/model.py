from couchdb.schema import *
from datetime import datetime
from decimal import Decimal
from exception import AlreadyVoidedException

class Doc(Document):
    created = DateTimeField()
    createdBy = TextField()
    modified = DateTimeField()
    modifiedBy = TextField()
    voided = DateTimeField()
    voidedBy = TextField()
    type = TextField()

    def __init__(self, id=None, **values):
        super(Doc, self).__init__(id, **values)
        self.type = self.__class__.__name__.lower()

    def store(self, db):
        if self.created is None:
            self.created = datetime.now()
        else:
            self.modified = datetime.now()
        super(Doc, self).store(db)

    def void(self, db):
        if self.voided is not None:
            raise AlreadyVoidedException('%s: %s' % (self.type, self.id))
        else:
            self.voided = datetime.now()
            self.store(db)

class Item(Doc):
    nama = TextField()
    kategori = TextField()
    harga = DecimalField()

class Kategori(Doc):
    nama = TextField()
    parent = TextField()

class Menu(Doc):
    nama = TextField()
    kategori = TextField()
    harga = DictField(Schema.build(
        normal= DecimalField(),
        member= DecimalField()))

class Order(Doc):
    meja = TextField()          # no meja atau takeout
    waiter = TextField()
    waktuPesan = DateTimeField(default=datetime.now)
    details = ListField(DictField(Schema.build(
        id = TextField(),
        nama = TextField(),
        harga = DecimalField(),
        qty = DecimalField(),
        keterangan = TextField(),
        voided = DateTimeField(),
        voidedBy = TextField(),
        modified = DateTimeField(),
        modifiedBy = TextField())))
    pembayaranID = TextField()
    pembayaranRev = TextField()

    def tagihanSebelumPajak(self):
        return reduce(lambda x, y: x + (y.harga * y.qty), self.details, Decimal(0))

    def jumlahTagihan(self):
        return self.tagihanSebelumPajak() + self.pajak()

    def pajak(self):
        return self.tagihanSebelumPajak() * Decimal('0.1')


class Pembayaran(Doc):
    orderID = TextField()
    orderRev = TextField()
    waktuBayar = DateTimeField()
    cashier = TextField()
    member = TextField()                        # no anggota member
    details = ListField(DictField(Schema.build(
        caraBayar = TextField(),                # tunai, voucher, kartu kredit
        noKartu = TextField(),                  # no voucher atau kartu kredit
        jenisKartu = TextField(),               # jenis kartu kredit mastercard/visa/american express
        bank = TextField(),                     # bank penerbit kartu kredit
        jumlah = DecimalField())))

    def jumlahBayar(self):
        return sum((x.jumlah for x in self.details))

class Meja(Doc):
    nama = TextField()

class User(Doc):
    nama = TextField()
    group = TextField()
    password = TextField()

class Group(Doc):
    nama = TextField()
    parent = TextField()

class Permission(Doc):
    subject = TextField()
    action = TextField()
    object = TextField()
