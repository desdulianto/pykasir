from datetime import datetime
from utils import noneOrEmpty
import model
import couchdb

class CRUDObject(object):
    def __init__(self, table, database, connection):
        self.mdl = model
        self.database = database
        self.connection = connection
        self.db = self.connection[self.database]
        self.modelObj = getattr(self.mdl, table)

        # hack untuk mendapatkan jenis model
        self.type = self.modelObj().type

    def _validate(self, **data):
        '''validasi field untuk data'''
        for key in self._validationFuncs:
            # validasi untuk data required
            if self._validationFuncs[key][0] is True and key not in data:
                raise Exception('%s is required' % key)
            if key in data:
                if not self._validationFuncs[key][1](data[key]):
                    raise Exception('%s is error' % key)

    def _validateCreate(self, model):
        '''validasi model yang akan ditambahkan ke database
           akan didefinisikan pada program client untuk menvalidasi
           masing-masing model yang akan ditambahkan ke database'''
        if model.id.strip() == '':
            raise Exception('ID tidak boleh kosong')

    def _preCreate(self, model):
        '''fungsi callback dipanggil sesaat sebelum document disimpan 
            ke database (pada fungsi create)
            fungsi ini secara default tidak melakukan apa-apa. program
            client dapat mendefinisikan pada fungsi ini seluruh proses yang 
            akan dijalankan, seperti validasi data dan untuk melengkapi data
            pada document'''
        pass

    def _postCreate(self, model):
        '''fungsi callback dipanggil setelah document disimpan ke database
            (pada fungsi create)
            fungsi ini secara default tidak melakukan apa-apa. program
            client dapat mendefinisikan pada fungsi ini seluruh proses yang
            akan dijalankan, seperti mengupdate document lain yang berkaitan
            dst'''
        pass

    #def _validateUpdate(self, model):
    #    pass

    #def _validateDelete(self, model):
    #    pass

    def create(self, **data):
        '''tambahkan document baru'''

        # konversi json menjadi document couchdb
        item = self.modelObj(**data)

        # proses sebelum document disimpan
        self._preCreate(item)

        # seluruh document yang dibentuk harus memiliki id
        if noneOrEmpty(item, 'id'):
            raise Exception('ID tidak boleh kosong')

        # tidak boleh ada data modified, voided pada document
        # yang baru dibuat
        item.modified = None
        item.modifiedBy = None
        item.voided = None
        item.voidedBy = None

        # simpan
        item.store(self.db)

        # proses setelah document disimpan
        self._postCreate(item)
        return (item.id, item.rev)

    def read(self, designDoc, **options):
        '''baca document dari couchdb'''
        #options.update(dict(include_docs=True))
        return self.modelObj.view(self.db, designDoc, **options)

    def readOne(self, id):
        '''baca document dari couchdb
            id -- id document yang ingin dibaca
            return Document jika document yang dibaca dapat ditemukan
            return None jika document yang dibaca tidak dapat ditemukan
        '''
        return self.modelObj.load(self.db, id)

    def update(self, id, rev, **data):
        '''update document couchdb'''
        item = self.modelObj.load(self.db, id)
        # document telah diupdate oleh proses lain
        # FIXME: should raise exception
        if item.rev != rev:
            raise couchdb.ResourceConflict()

        # update data
        for field in data:
            setattr(item, field, data[field])

        # validasi data
        #self._validate(item.unwrap())
        self._preCreate(item)

        # simpan
        item.store(self.db)
        return (item.id, item.rev)

    def delete(self, id, rev):
        '''meng-void document couchdb
            fungsi delete TIDAK MENGHAPUS document couchdb,
            hanya menandakan bahwa document telah di void dengan
            mengisi field voided'''
        item = self.modelObj.load(self.db, id)
        # jika document yang dimaksud sudah mengalami perubahan
        # batalkan void
        # FIXME: should raise exception
        if item.rev != rev:
            return None
        item.void(self.db)
        return (item.id, item.rev)

#pembayaran = f(Pembayaran, Order, 'chatterbox', dbconn)
#pembayaran.create(....):
#    get order id
#    jika order id sudah ada pembayaran error
#    jika tidak tambahkan dan simpan data pembayaran
#    pasangkan id pembayaran ke order
#    update data order dengan id pembayaran
#pembayaran.delete(....):
#    void pembayaran, simpan
#    hapus data pembayaran pada order, simpan
